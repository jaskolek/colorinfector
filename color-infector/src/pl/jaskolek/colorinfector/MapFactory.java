package pl.jaskolek.colorinfector;

import java.util.Random;

import com.sun.org.apache.bcel.internal.generic.GETSTATIC;

import pl.jaskolek.colorinfector.actors.Field;
import pl.jaskolek.colorinfector.actors.Map;
import pl.jaskolek.colorinfector.actors.Piece;
import pl.jaskolek.colorinfector.actors.piecefragments.AdjacentInfector;

public class MapFactory {

	public static Map randomMap(int rows, int cols, int seed) {
		Map map = new Map(rows, cols);
		Random random = new Random(seed);

		return map;
	}

	public static Map testMap() {
		Map map = new Map(5, 5);

		Field field = map.getField(1, 1);
		Piece piece = new Piece(map.getShapeRenderer());

		piece.addFragment(new AdjacentInfector(map.getShapeRenderer(),
				AdjacentInfector.Direction.TOP));
		piece.addFragment(new AdjacentInfector(map.getShapeRenderer(),
				AdjacentInfector.Direction.TOP_LEFT));
		piece.addFragment(new AdjacentInfector(map.getShapeRenderer(),
				AdjacentInfector.Direction.LEFT));
		piece.addFragment(new AdjacentInfector(map.getShapeRenderer(),
				AdjacentInfector.Direction.BOTTOM_LEFT));
		piece.addFragment(new AdjacentInfector(map.getShapeRenderer(),
				AdjacentInfector.Direction.BOTTOM));
		piece.addFragment(new AdjacentInfector(map.getShapeRenderer(),
				AdjacentInfector.Direction.BOTTOM_RIGHT));
		piece.addFragment(new AdjacentInfector(map.getShapeRenderer(),
				AdjacentInfector.Direction.RIGHT));
		piece.addFragment(new AdjacentInfector(map.getShapeRenderer(),
				AdjacentInfector.Direction.TOP_RIGHT));

		field.setPiece(piece);
		return map;
	}
	/*
	 * public static Piece createRandomPiece(){ Piece piece = new Piece();
	 * 
	 * }
	 */
}
