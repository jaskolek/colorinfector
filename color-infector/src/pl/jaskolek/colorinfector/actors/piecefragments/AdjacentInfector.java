package pl.jaskolek.colorinfector.actors.piecefragments;

import pl.jaskolek.colorinfector.actors.PieceFragment;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

public class AdjacentInfector extends PieceFragment {

	public enum Direction {
		TOP_LEFT, TOP, TOP_RIGHT, LEFT, RIGHT, BOTTOM_LEFT, BOTTOM, BOTTOM_RIGHT
	};

	private Direction direction;
	private ShapeRenderer shapeRenderer;

	public AdjacentInfector(ShapeRenderer shapeRenderer, Direction direction) {
		super();

		this.shapeRenderer = shapeRenderer;
		setDirection(direction);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		setOrigin(getWidth()/2, getHeight()/2);
		
		Vector2 point1 = new Vector2(getWidth() *0.5f,getHeight() * 0.5f);
		Vector2 point2 = new Vector2(getWidth(), getHeight() * 0.32f);
		Vector2 point3 = new Vector2(getWidth(), getHeight() * 0.68f);

		localToStageCoordinates(point1);
		localToStageCoordinates(point2);
		localToStageCoordinates(point3);

		shapeRenderer.triangle(point1.x, point1.y, point2.x, point2.y,
				point3.x, point3.y);

		super.draw(batch, parentAlpha);
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;

		switch (this.direction) {
		case RIGHT:
			setRotation(0);
			break;

		case TOP_RIGHT:
			setRotation(45);
			break;

		case TOP:
			setRotation(90);
			break;

		case TOP_LEFT:
			setRotation(135);
			break;

		case LEFT:
			setRotation(180);
			break;

		case BOTTOM_LEFT:
			setRotation(225);
			break;

		case BOTTOM:
			setRotation(270);
			break;

		case BOTTOM_RIGHT:
			setRotation(315);
			break;

		}

	}
}
