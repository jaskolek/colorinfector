package pl.jaskolek.colorinfector.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Group;

public class Field extends Group {
	ShapeRenderer shapeRenderer;
	
	Piece piece = null;
	Map map = null;
	
	public Field(ShapeRenderer shapeRenderer) {
		super();
		this.shapeRenderer = shapeRenderer;
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		
		shapeRenderer.setColor(Color.WHITE);
		shapeRenderer.rect(getX(), getY(), getWidth(), getHeight());
		super.draw(batch, parentAlpha);
	}

	public Piece getPiece() {
		return piece;
	}

	public void setPiece(Piece piece) {
		this.piece = piece;
		
		piece.setSize(getWidth(), getHeight());
		piece.setPosition(0, 0);
		piece.setOrigin(getWidth()/2, getHeight()/2);
		addActor(piece);
	}
	
	
}
