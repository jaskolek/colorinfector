package pl.jaskolek.colorinfector.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Array;

public class Piece extends Group {

	Array<PieceFragment> fragments = new Array<PieceFragment>();

	private Status currentStatus = new Status(false, true, false);
	private Status endStatus = new Status(false, false, false);

	private ShapeRenderer shapeRenderer;

	public Piece(ShapeRenderer shapeRenderer) {
		super();

		this.shapeRenderer = shapeRenderer;

		addListener(clickListener);
	}

	public void addFragment(PieceFragment fragment) {
		fragments.add(fragment);
		fragment.setSize(getWidth(), getHeight());
		fragment.setPosition(0, 0);
		fragment.setOrigin(getWidth()/2, getHeight()/2);
		
		addActor(fragment);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		shapeRenderer.setColor(currentStatus.getColor());
		super.draw(batch, parentAlpha);
	}

	
	class Status {
		public boolean hasRed;
		public boolean hasGreen;
		public boolean hasBlue;

		public Status() {
			super();
		}

		public Status(boolean hasRed, boolean hasGreen, boolean hasBlue) {
			super();
			this.hasRed = hasRed;
			this.hasGreen = hasGreen;
			this.hasBlue = hasBlue;
		}

		public Color getColor() {
			return new Color(hasRed ? 1 : 0, hasGreen ? 1 : 0, hasBlue ? 1 : 0,
					1);
		}
	}


	InputListener clickListener = new InputListener() {
		@Override
		public boolean touchDown(InputEvent event, float x, float y,
				int pointer, int button) {
			
			
			return super.touchDown(event, x, y, pointer, button);
		}

	};

	@Override
	protected void sizeChanged() {
		super.sizeChanged();
		
		for( int i = 0; i < fragments.size; ++i ){
			fragments.get(i).setSize(getWidth(), getHeight());
		}
	}
	
	
	
}
