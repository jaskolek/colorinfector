package pl.jaskolek.colorinfector.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;

public class Map extends Group {
	private Array<Array<Field>> fields;
	private ShapeRenderer shapeRenderer = new ShapeRenderer();

	public final static float MAX_SIZE = 480;
	public final static float FIELD_PADDING = 1;

	private int rows;
	private int cols;

	public Map(int rows, int cols) {
		super();
		this.rows = rows;
		this.cols = cols;
		fields = new Array<Array<Field>>(this.rows);
		for (int i = 0; i < this.rows; ++i) {
			fields.add(new Array<Field>(this.cols));
			for (int j = 0; j < this.cols; ++j) {
				//adding null - now we can use setField method
				fields.get(i).add(null);
				setField(i, j, new Field(shapeRenderer));
				addActor(getField(i, j));
			}
		}
	}

	private float calculateFieldSize() {
		float elements = Math.max(rows, cols);

		return (MAX_SIZE / elements) - FIELD_PADDING * 2;
	}

	/**
	 * ends batch, and starts shapeRenderer for fields
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.end();
		shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
		shapeRenderer.setTransformMatrix(batch.getTransformMatrix());
		shapeRenderer.begin(ShapeType.Filled);

		super.draw(batch, parentAlpha);
		shapeRenderer.end();

		batch.begin();
	}

	/**
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	public Field getField(int row, int col) {
		return fields.get(row).get(col);
	}

	/**
	 * removes field from map
	 * 
	 * @param row
	 * @param col
	 */
	public void removeField(int row, int col) {
		Field field = getField(row, col);
		removeActor(field);
		fields.get(row).removeIndex(col);
	}

	/**
	 * 
	 * @param row
	 * @param col
	 * @param field
	 */
	public void setField(int row, int col, Field field) {
		addActor(field);
		float size = calculateFieldSize();
		field.setWidth(size);
		field.setHeight(size);
		field.setX((size + FIELD_PADDING * 2) * col + FIELD_PADDING);
		field.setY((size + FIELD_PADDING * 2) * row + FIELD_PADDING);
		
		fields.get(row).set(col, field);
	}

	/**
	 * proxy for getField().setPiece()
	 * 
	 * @param row
	 * @param col
	 * @param piece
	 */
	public void setPiece(int row, int col, Piece piece) {
		getField(row, col).setPiece(piece);
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getCols() {
		return cols;
	}

	public void setCols(int cols) {
		this.cols = cols;
	}

	public ShapeRenderer getShapeRenderer() {
		return shapeRenderer;
	}

	public void setShapeRenderer(ShapeRenderer shapeRenderer) {
		this.shapeRenderer = shapeRenderer;
	}
	
	

}
