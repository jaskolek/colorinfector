package pl.jaskolek.colorinfector;

import pl.jaskolek.colorinfector.screens.GameScreen;
import pl.jaskolek.colorinfector.ui.BaseScreen;
import pl.jaskolek.colorinfector.ui.UiApp;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;

/**
 * Field width: 1000
 * @author jaskolek
 *
 */
public class ColorInfector extends UiApp {
	
	@Override
	public void create() {		
		generateAtlas();
		Assets.init( new TextureAtlas( Gdx.files.internal("data/assets.atlas") ) );

		super.create();
	}

	@Override
	public void dispose() {
	}

	@Override
	public void render() {	
		super.render();
	}


	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	protected String atlasPath() {
		return "data/uiskin.atlas";
	}

	@Override
	protected String skinPath() {
		return "data/uiskin.json";
	}

	@Override
	protected void styleSkin(Skin skin, TextureAtlas atlas) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected BaseScreen getFirstScreen() {
		return new GameScreen(this, skin);
	}
	

	private void generateAtlas(){
		TexturePacker2.process("../color-infector-android/assets/raw", "../color-infector-android/assets/data", "assets.atlas");
	}
	
}
