package pl.jaskolek.colorinfector.screens;

import pl.jaskolek.colorinfector.MapFactory;
import pl.jaskolek.colorinfector.actors.Map;
import pl.jaskolek.colorinfector.ui.BaseScreen;
import pl.jaskolek.colorinfector.ui.UiApp;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class GameScreen extends BaseScreen {

	public static final float SCREEN_RATIO = 0.6f;
	Skin skin;
	Map map = null;

	public GameScreen(UiApp app, Skin skin) {
		super(app);
		this.skin = skin;

		setMap(MapFactory.testMap());
	}

	@Override
	public void onBackPress() {
		// TODO Auto-generated method stub
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		if( map != null ){
			removeActor(map);
		}
	
		this.map = map;
		addActor(map);
	}
	
	
}
